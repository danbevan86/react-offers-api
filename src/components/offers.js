import React from 'react'

const Offers = ({offers}) => {
    return (
        <div>
            <center><h1>Offers List</h1></center>
            {offers.map((offer) => (
                <div className="card" key={offer.id}>
                    <div className="card-body">
                        <img src={offer.merchant.logo_url} alt={offer.merchant.name} />
                        <h5 className="card-title">{offer.merchant.name}</h5>
                        <h6 className="card-subtitle mb-2 text-muted">{offer.model}</h6>
                        <p className="card-text">{offer.offer.currency_symbol}{offer.offer.price}</p>
                        <a href={offer.offer.link}>View Affiliate Link</a>
                    </div>
                </div>
            ))}
        </div>
    )
};

export default Offers