import React, {Component} from 'react';
import Offers from './components/offers';

class App extends Component {
    render() {
        return (
            <Offers offers={this.state.offers} />
        )
    }

    state = {
        offers: []
    };

    componentDidMount() {
        fetch('http://search-api.fie.future.net.uk/widget.php?id=review&site=TRD&model_name=iPad_Air')
			.then(res => res.json())
			.then((data) => {
                this.setState({ offers: data.widget.data.offers })
            })
            .catch(console.log)
    }
}

export default App;
